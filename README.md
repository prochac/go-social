# services
- NATS
- gateway(GraphQL/gRPC)
- messaging(WebSocket)
- Auth for login/session storage(badger)
- User profile/posts/photos storage(cockroachDB/dgraph)
- file storage(min.io)
- relationship service?
- feeds service(scylla)

# queries/mutations
- (M)register
- (M)login
- (M)logout
- (M)forgot pass request
- (M)change pass

- (M)send post
- (M)send friend request
- (M)approve friend request
- (M)like post
- (M)send comment

- (Q)get feed
- (Q)show profile
- (Q)show friend requests

# messaging
- send message
- get message
