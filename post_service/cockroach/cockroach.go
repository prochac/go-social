package cockroach

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

func PingDB(dsn string) error {
	db, err := sqlx.Open("postgres", dsn)
	if err != nil {
		return errors.Wrap(err, "Failed to connect to database")
	}
	defer db.Close()

	var num int
	if err := db.Get(&num, "SELECT 42"); err != nil {
		return errors.Wrap(err, "Failed to SELECT")
	}

	fmt.Println(num)
	return nil
}
