package main

import (
	"app/cockroach"
	"app/grpc"
	"app/nats"
	"log"
	"os"

	"github.com/pkg/errors"
)

func main() {
	if err := Run(); err != nil {
		log.Fatal(err)
	}
}

func Run() error {
	dbDsn, _ := os.LookupEnv("DATABASE_DSN")
	if err := cockroach.PingDB(dbDsn); err != nil {
		return errors.Wrap(err, "Failed to ping database")
	}

	strDsn, _ := os.LookupEnv("STREAMING_DSN")
	if err := nats.Listen(strDsn); err != nil {
		return errors.Wrap(err, "Failed to connect to NATS")
	}

	if err := grpc.StartServer(); err != nil {
		return errors.Wrap(err, "Failed to start grpc server")
	}

	return nil
}
