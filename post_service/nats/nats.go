package nats

import (
	"fmt"

	"github.com/nats-io/nuid"
	"github.com/nats-io/stan.go"
	"github.com/pkg/errors"
)

func Listen(dsn string) error {
	sc, err := stan.Connect(
		"event_store",
		nuid.Next(),
		stan.NatsURL(dsn),
		stan.SetConnectionLostHandler(func(_ stan.Conn, reason error) {
			panic(reason)
		}),
	)
	if err != nil {
		return errors.Wrapf(err, "Failed to initiate NATS connection to %s", dsn)
	}
	defer sc.Close()

	sub, err := sc.Subscribe("foo", func(msg *stan.Msg) {
		fmt.Println(msg.String())
	}, stan.StartWithLastReceived())
	if err != nil {
		return errors.Wrap(err, "Failed to subscribe foo")
	}
	defer sub.Close()

	//for i := 0; i < 10; i++ {
	//	if err := sc.Publish("foo", []byte("bar")); err != nil {
	//		return errors.Wrap(err, "Failed to publish bar to foo")
	//	}
	//}

	return nil
}
