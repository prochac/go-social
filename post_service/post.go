package post_service

import (
	"fmt"
	"time"

	"github.com/altairsix/eventsource"
)

type PostCreated struct {
	eventsource.Model
	Title string
	Text  string
}

type PostTitleChanged struct {
	eventsource.Model
	Title string
}

type PostTextChanged struct {
	eventsource.Model
	Text string
}

type PostDeleted struct {
	eventsource.Model
}

type Post struct {
	ID      string
	Title   string
	Text    string
	Created time.Time
	Updated time.Time
}

func (p *Post) On(event eventsource.Event) error {
	switch v := event.(type) {
	case PostCreated:
		p.Created = v.At
		p.Updated = v.At
		p.Title = v.Title
		p.Text = v.Text
	case PostTitleChanged:
		p.Updated = v.At
		p.Title = v.Title
	case PostTextChanged:
		p.Updated = v.At
		p.Text = v.Text
	case PostDeleted:
		*p = Post{}

	default:
		return fmt.Errorf("unhandled event, %v", v)
	}

	return nil
}
