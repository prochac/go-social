module post_service

go 1.12

require (
	github.com/altairsix/eventsource v0.0.0-20170815104732-7b6859b7a009
	github.com/golang/protobuf v1.3.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.1.1
	github.com/nats-io/gnatsd v1.4.1 // indirect
	github.com/nats-io/go-nats v1.7.2 // indirect
	github.com/nats-io/nats-server v1.4.1 // indirect
	github.com/nats-io/nats-streaming-server v0.15.1 // indirect
	github.com/nats-io/nuid v1.0.1
	github.com/nats-io/stan.go v0.4.5
	github.com/pkg/errors v0.8.1
	google.golang.org/grpc v1.22.1
)
