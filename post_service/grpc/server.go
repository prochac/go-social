package grpc

import (
	"app/pb"
	"context"
	"fmt"
	"net"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

type PostServer struct {
}

func (p PostServer) CreatePost(context.Context, *pb.CreatePostRequest) (*empty.Empty, error) {
	panic("implement me")
}

func (p PostServer) ChangeTitle(context.Context, *pb.ChangeTitleRequest) (*empty.Empty, error) {
	panic("implement me")
}

func (p PostServer) ChangeText(context.Context, *pb.ChangeTextRequest) (*empty.Empty, error) {
	panic("implement me")
}

func (p PostServer) DeletePost(context.Context, *pb.DeletePostRequest) (*empty.Empty, error) {
	panic("implement me")
}

func StartServer() error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":9091"))
	if err != nil {
		return errors.Wrap(err, "Failed to listen")
	}
	server := grpc.NewServer()
	pb.RegisterPostServiceServer(server, &PostServer{})

	if err := server.Serve(lis); err != nil {
		return errors.Wrap(err, "")
	}

	return nil
}
