module oauth2

go 1.12

require (
	github.com/go-session/session v3.1.2+incompatible
	gopkg.in/oauth2.v3 v3.10.0
)
