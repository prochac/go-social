package main

import (
	"log"
	"oauth2"
)

func main() {
	if err := oauth2.Run(); err != nil {
		log.Fatalf("%#v", err)
	}
}
