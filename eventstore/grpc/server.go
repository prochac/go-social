package grpc

import (
	"context"
	"fmt"
	"net"
	"store/cockroach"
	"store/nats"

	"github.com/cockroachdb/errors"
	"google.golang.org/grpc"

	"store/pb"
)

type EventStoreServer struct {
	db   *cockroach.DB
	nats *nats.Client
}

func (e *EventStoreServer) CreateEvent(context.Context, *pb.Event) (*pb.Response, error) {
	panic("implement me")
}

type Server struct {
	grpc *grpc.Server
}

func NewServer(db *cockroach.DB, nats *nats.Client) *Server {
	server := grpc.NewServer()
	pb.RegisterEventStoreServer(server, &EventStoreServer{db: db, nats: nats})

	return &Server{grpc: server}
}

func (s *Server) Serve() error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":9090"))
	if err != nil {
		return errors.Wrap(err, "Failed to listen")
	}

	return s.grpc.Serve(lis)
}
