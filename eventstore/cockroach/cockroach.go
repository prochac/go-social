package cockroach

import (
	"github.com/Masterminds/squirrel"
	"github.com/cockroachdb/errors"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"

	"store"
)

type DB struct {
	db *sqlx.DB
}

func Open(dsn string) (*DB, error) {
	db, err := sqlx.Open("postgres", dsn)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to connect to database")
	}

	return &DB{db: db}, nil
}

func (db *DB) Close() error {
	return db.db.Close()
}

func (db *DB) Ping() error {
	var num int
	if err := db.db.Get(&num, "SELECT 42"); err != nil {
		return errors.Wrap(err, "Failed to exec SELECT query")
	}

	return nil
}

func (db *DB) Create(event eventstore.Event) error {
	query, args, err := squirrel.Insert("events").ToSql()
	if err != nil {
		return errors.Wrap(err, "Failed to build query")
	}

	if _, err := db.db.Exec(query, args...); err != nil {
		return errors.Wrap(err, "Failed to exec SELECT query")
	}

	return nil
}
