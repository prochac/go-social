package nats

import (
	"log"
	"store/pb"

	"github.com/cockroachdb/errors"
	"github.com/golang/protobuf/proto"
	"github.com/nats-io/nuid"
	"github.com/nats-io/stan.go"

	"store"
)

type Client struct {
	nats stan.Conn
}

func Connect(dsn string) (*Client, error) {
	sc, err := stan.Connect(
		"event_store",
		nuid.Next(),
		stan.NatsURL(dsn),
		stan.SetConnectionLostHandler(func(_ stan.Conn, reason error) {
			panic(reason)
		}),
	)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to initiate NATS connection to %s", dsn)
	}

	return &Client{nats: sc}, nil
}

func (c *Client) Close() error {
	return c.nats.Close()
}

type Subscription interface {
	stan.Subscription
}

func (c *Client) Subscribe(subject string, cb func(event eventstore.Event)) (Subscription, error) {
	sub, err := c.nats.Subscribe(subject, func(msg *stan.Msg) {
		var event pb.Event
		if err := proto.Unmarshal(msg.Data, &event); err != nil {
			log.Panic("unmarshaling error: ", err) // TODO handle better?
		}
		cb(eventstore.Event{
			ID:            event.Id,
			Type:          event.Type,
			AggregateID:   event.AggregateId,
			AggregateType: event.AggregateType,
			Data:          event.Data,
		})
	}, stan.StartWithLastReceived())
	if err != nil {
		return nil, errors.Wrap(err, "Failed to subscribe foo")
	}

	return sub, nil
}

func (c *Client) Publish(event eventstore.Event) error {
	b, err := proto.Marshal(&pb.Event{
		Id:            event.ID,
		Type:          event.Type,
		AggregateId:   event.AggregateID,
		AggregateType: event.AggregateType,
		Data:          event.Data,
	})
	if err != nil {
		return errors.Wrap(err, "Failed to marshal event")
	}

	if err := c.nats.Publish(event.Type, b); err != nil {
		return errors.Wrap(err, "Failed to publish event")
	}
	return nil
}
