package main

import (
	"log"
	"os"

	"github.com/cockroachdb/errors"

	"store/cockroach"
	"store/grpc"
	"store/nats"
)

func main() {
	if err := Run(); err != nil {
		log.Fatal(err)
	}
}

func Run() error {
	dbDsn, _ := os.LookupEnv("DATABASE_DSN")

	db, err := cockroach.Open(dbDsn)
	if err != nil {
		return errors.Wrap(err, "Failed to open database")
	}
	defer db.Close()

	if err := db.Ping(); err != nil {
		return errors.Wrap(err, "Failed to ping database")
	}

	strDsn, _ := os.LookupEnv("STREAMING_DSN")
	c, err := nats.Connect(strDsn)
	if err != nil {
		return errors.Wrap(err, "Failed to connect to NATS")
	}

	s := grpc.NewServer(db, c)
	if err := s.Serve(); err != nil {
		return errors.Wrap(err, "Failed to start grpc server")
	}

	return nil
}
