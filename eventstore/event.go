package eventstore

type Event struct {
	ID            string `db:"id"`
	Type          string `db:"type"`
	AggregateID   string `db:"aggregate_id"`
	AggregateType string `db:"aggregate_type"`
	Data          string `db:"data"`
}
