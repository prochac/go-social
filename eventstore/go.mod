module store

go 1.12

require (
	github.com/Masterminds/squirrel v1.1.0
	github.com/certifi/gocertifi v0.0.0-20190506164543-d2eda7129713 // indirect
	github.com/cockroachdb/errors v1.2.3
	github.com/cockroachdb/logtags v0.0.0-20190617123548-eb05cc24525f // indirect
	github.com/getsentry/raven-go v0.2.0 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.1.1
	github.com/nats-io/nats-server/v2 v2.0.2 // indirect
	github.com/nats-io/nats-streaming-server v0.15.1 // indirect
	github.com/nats-io/nuid v1.0.1
	github.com/nats-io/stan.go v0.5.0
	google.golang.org/grpc v1.22.1
)
